<?php

namespace Comp\bitm\seipmini\userreg;

class profile {
    private $con='';
    public $userid=NULL;
    public $id=NULL;
    public $firstName=NULL;
    public $lastName=NULL;
    public $personalPhone=NULL;
    public $homePhone='';
    public $officePhone=NULL;
    public $currentAddress=NULL;
    public $permanentAddress=NULL;
    public $profile_pic=NULL;

    public function __construct()
    {
        if($this->con=mysqli_connect("localhost","root","")){
            if(mysqli_select_db($this->con,"usrreg")){
                //echo 'DB Connected';
            }
            else{
                echo 'db error';
            }
        }else{
            echo 'connect erro';
        }
    }
    public function prepare($val){
        if(isset($val['username']) and !empty($val['username'])){
           // $this->username=$val['username'];
            $this->username=mysqli_real_escape_string($this->con,$val['username']);
        }
        if(isset($val['email']) and !empty($val['email'])){
            //$this->email=$val['email'];
            $this->email=mysqli_real_escape_string($this->con,$val['email']);
        }
         if(isset($val['password']) and !empty($val['password'])){
            //$this->email=$val['email'];
            $this->password=mysqli_real_escape_string($this->con,$val['password']);
        }
        if(isset($val['id']) and !empty($val['id'])){
            $this->id=$val['id'];
        }
        if(isset($val['unique_id']) and !empty($val['unique_id'])){
            //$this->email=$val['email'];
            $this->unique_id=mysqli_real_escape_string($this->con,$val['unique_id']);
        }
        if(isset($val['first_name']) and !empty($val['first_name'])){
            //$this->email=$val['email'];
            $this->firstName=mysqli_real_escape_string($this->con,$val['first_name']);
        }
        if(isset($val['last_name']) and !empty($val['last_name'])){
            //$this->email=$val['email'];
            $this->lastName=mysqli_real_escape_string($this->con,$val['last_name']);
        }
        if(isset($val['personal_phone']) and !empty($val['personal_phone'])){
            //$this->email=$val['email'];
            $this->personalPhone=mysqli_real_escape_string($this->con,$val['personal_phone']);
        }
        if(isset($val['home_phone']) and !empty($val['home_phone'])){
            //$this->email=$val['email'];
            $this->homePhone=mysqli_real_escape_string($this->con,$val['home_phone']);
        }
        if(isset($val['office_phone']) and !empty($val['office_phone'])){
            //$this->email=$val['email'];
            $this->officePhone=mysqli_real_escape_string($this->con,$val['office_phone']);
        }
        if(isset($val['current_address']) and !empty($val['current_address'])){
            //$this->email=$val['email'];
            $this->currentAddress=mysqli_real_escape_string($this->con,$val['current_address']);
        }
        if(isset($val['permanent_address']) and !empty($val['permanent_address'])){
            //$this->email=$val['email'];
            $this->permanentAddress=mysqli_real_escape_string($this->con,$val['permanent_address']);
        }
        if(isset($val['imageName']) and !empty($val['imageName'])){
            //$this->email=$val['email'];
            $this->profile_pic=mysqli_real_escape_string($this->con,$val['imageName']);
        }

       
    }
    public function storeId($id){
            $sql="INSERT INTO `usrreg`.`profiles` ( `user_id`) VALUES ('".$id."')";
            if(mysqli_query($this->con,$sql)){
                session_start();
                $_SESSION['registered']='Successfully Registered';
                    header('location:create.php');             

            }
            else{
                echo 'profile error';
            }
            
        }
    public function deleteUser(){
        $sql="DELETE FROM `usrreg`.`profiles` WHERE `profiles`.`user_id` =".$this->id;
        if(mysqli_query($this->con,$sql)){
            session_start();
            $_SESSION['delUserID']=$this->id;
            header('location:delete.php');
        }
        else{
            //echo 'query prolem';
        }
    }

    public function view(){
        $sql="SELECT * FROM `profiles` WHERE user_id=" . $this->id;
        $res=mysqli_query($this->con,$sql);
        $Details=  mysqli_fetch_assoc($res);
        return $Details;
    }
    public function LoginPageReDr(){

    }
    public function profileUpdate(){
        $sql="UPDATE `usrreg`.`profiles` SET `first_name` = '".$this->firstName."', `last_name` = '".$this->lastName."', `personal_phone` = '".$this->personalPhone."', `home_phone` = '".$this->homePhone."', `office_phone` = '".$this->officePhone."', `current_address` = '".$this->currentAddress."', `permanent_address` = '".$this->permanentAddress."' WHERE `profiles`.`user_id` =".$this->id;
        //echo $sql;die();
        if(mysqli_query($this->con,$sql)){
            session_start();
            $_SESSION['updateProfile']='<span style="color:green">Successfully updated</span>';
            header('location:profile.php');
        }
    }
    public function profilePicUp(){
        $sql="UPDATE `usrreg`.`profiles` SET  `profile_pic` = '".$this->profile_pic."' WHERE `profiles`.`user_id` =".$this->id;
        if(mysqli_query($this->con,$sql)){
            session_start();
            $_SESSION['updateProfile']='<span style="color:green">Successfully updated</span>';
            header('location:profile.php');
        }


    }

}
