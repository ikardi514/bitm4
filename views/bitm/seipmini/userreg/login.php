<?php
//include_once 'design.php';
session_start();

if(isset($_SESSION['useridlogin'])){
    if(isset($_SESSION['is_admin'])){
        header('location:adminDashboard.php');
    }else{
        header('location:dashboard.php');
    }



}
?>
<html>
    <head>
        <link href="../../../../assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="../../../../assets/css/style.css" rel="stylesheet">
        <script src="../../../../assets/js/bootstrap.min.js"></script>
        <script src="../../../../assets/js/scripts.js"></script>
		
		
		<style>
		#middle1{
			width:100%;
			min-height:600px;
			background:#CC99FF;
			margin:10px auto;
			
		}
		</style>
		
		

    </head>
    <body>
	
	
	<div id="middle1">
    <br/> <br/> <br/>
    <div class="col-md-12">
        <div class="col-md-4"></div>
            <div class="col-md-4">
                <?php
                if(isset($_SESSION['failed'])){?>
                <div class="alert alert-success" role="alert">
                    <strong><?php echo $_SESSION['failed']; ?>!</strong>.
                </div>

                 <?php
                    unset($_SESSION['failed']);
                }
                ?>


            <legend ><h1>Login</h1></legend>
            <form action="logindone.php" role="form" method="post">

                <div class="form-group">
                <label>User Name</label>
                <input type="text" class="form-control" required name="username"/>
                </div>

                
                <div class="form-group">
                <label>Your Password</label>
                <input type="password" class="form-control"required name="password"/>
                </div>
                
                <input type="submit" name="btn"  class="btn-success" value="Login"/>
                <a href="create.php" class="alert-link"><label>For Registration</label></a>

            </form>

                </div>
    </div>
    </body>
</html>
