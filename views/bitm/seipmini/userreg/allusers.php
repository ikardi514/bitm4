<?php
session_start();
if(!isset($_SESSION['useridlogin'])){
    header('location:login.php');
}
include "../../../../vendor/autoload.php";
use Comp\bitm\seipmini\userreg\userreg;
use Comp\bitm\seipmini\userreg\profile;

$userObj=new userreg();
$alldata=$userObj->allNormalUser();
//print_r($alldata);

?>
<html>
    <head>
        <script src="../../../../assets/jquery.js"></script>
        <link href="../../../../assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="../../../../assets/css/style.css" rel="stylesheet">
        <script src="../../../../assets/js/bootstrap.min.js"></script>
        <script src="../../../../assets/js/scripts.js"></script>

    </head>

    <body>
    <div class="col-md-12">
        <div class="col-md-3"></div>
            <div class="col-md-6">
                <center>
                    <br/><br/><br/><br/>
                    <a href="logout.php">
                        <button class="btn btn-default" type="button">
                            <em class="glyphicon glyphicon-log-out"></em> Logout
                        </button>
                    </a>
                    <a href="profile.php">
                        <button class="btn btn-default" type="button">
                            <em class="glyphicon glyphicon-certificate"></em> Your Profile
                        </button>
                    </a>
                    <a href="logindone.php">
                        <button class="btn btn-default" type="button">
                            <em class="glyphicon glyphicon-home"></em> Home
                        </button>
                    </a>
                    <br/><br/><br/><br/>

                    <table class="table table-bordered table-hover table-responsive">
                        <tr bgcolor="#1e90ff">
                            <td>UserName</td>
                            <td>Email</td>
                            <td>Action/Active</td>
                        </tr>
                        <?php foreach($alldata as $data){?>
                        <tr>
                            <td><?php echo $data['username'];?></td>
                            <td><?php echo $data['email'];?></td>
                            <td>
                                <a href="delete.php?id=<?php echo $data['id'];?>"> Delete user</a>|
                                <?php if(isset($data['is_active'])){ ?>
                                    <a href="deactive.php?id=<?php echo $data['id'];?>"><input type="radio" checked name="active<?php echo $data['id'];?>" style="pointer-events: none"></a>
                                <?php  }else{?>
                            <a href="active.php?id=<?php echo $data['id'];?>"><input type="radio" name="active<?php echo $data['id'];?>" style="pointer-events: none"></a>
                                <?php }?>
                            </td>
                        </tr>
                        <?php }?>

                    </table>
                </center>
            </div>
        <div class="col-md-3"></div>

    </div>
    </body>
</html>

