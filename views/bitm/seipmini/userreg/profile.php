<?php
session_start();
if(!isset($_SESSION['useridlogin'])){
    header('location:login.php');
}

include "../../../../vendor/autoload.php";
use Comp\bitm\seipmini\userreg\userreg;
use Comp\bitm\seipmini\userreg\profile;

$profileObj=new profile();
$val['id']=$_SESSION['useridlogin'];
$profileObj->prepare($val);

$alldata=$profileObj->view();

//print_r($alldata); die();

?>
<html>
<head>
    <title>User|Profile</title>
    <script src="../../../../assets/jquery.js"></script>

    <style>
        .profile {
            background-color: white;
            color:black;
            border-style: none;
        }
        #profileIMG{
            height: 150px;
            border-radius: 50%;
        }
		#middle{
			width:500px;
			min-height:500px;
			background:#CCCCFF;
			margin:20px auto;
			
		}

        #middle span {
            width: 100%;
            display: inline-block;
            marign:0px auto;
            text-align: center;
        }
        h2{
            text-align: center;
        }
        #imgg{
            text-align: center;
         }



    </style>
</head>
<body>

<div id="middle">
    <span>
<a href="logout.php">Logout</a> |
<a href="logindone.php">Home</a>
    </span>

<br/>
        <?php if(isset($_SESSION['updateProfile'])){
            echo $_SESSION['updateProfile'];
            unset($_SESSION['updateProfile']);
        } ?>
<h2>Your Profile Information</h2>
    <div id="imgg">
        <form action="imgUp.php" method="post" enctype="multipart/form-data">
            <div id="profilePicPreview">
                <?php
                if(isset($alldata['profile_pic']) && !empty($alldata['profile_pic'])) {
                    echo '<img id="profileIMG" src="../../../../img/' . $alldata['profile_pic'] . '" />';
                }else{
                ?>

           <img id="profileIMG"  src="../../../../img/upload.jpg">
                <?php } ?>

            </div>
            <input type="file" hidden id="profilePicUp" name="image">
            <div id="submitPic"></div>
            <input type="text" hidden name="oldImg" value="<?php echo $alldata['profile_pic']; ?>"/>


        </form>
    </div>

    <div style="width: 296px;
            margin: 0px auto">
    <form action="profileUpdate.php" method="post">
        <table>
            <tr>
                <td>First Name</td>
                <td>:</td>
                <td><input type="text" class="profile profilevalue" id="firstName" name="first_name" value="<?php if(isset($alldata['first_name'])){ echo $alldata['first_name']; }?>" disabled/></td>
                <td id="fnm"></td>

            </tr>
            <tr>
                <td>Last Name</td>
                <td>:</td>
                <td><input type="text" class="profile profilevalue" id="lastName" name="last_name" value="<?php if(isset($alldata['last_name'])){ echo $alldata['last_name']; }?>" disabled/></td>
                <td id="lnm"></td>
            </tr>
            <tr>
                <td>Personal Phone</td>
                <td>:</td>
                <td><input type="text" class="profile profilevalue" id="personalPhone" name="personal_phone" value="<?php if(isset($alldata['personal_phone'])){ echo $alldata['personal_phone']; }?>" disabled/></td>
                <td id="ppm"></td>
            </tr>
            <tr>
                <td>Home Phone</td>
                <td>:</td>
                <td><input type="text" class="profile profilevalue" id="homePhone" name="home_phone" value="<?php if(isset($alldata['home_phone'])){ echo $alldata['home_phone']; }?>" disabled/></td>
                <td id="hpm"></td>
            </tr>
            <tr>
                <td>Office Phone</td>
                <td>:</td>
                <td><input type="text" class="profile profilevalue" id="officePhone" name="office_phone" value="<?php if(isset($alldata['office_phone'])){ echo $alldata['office_phone']; }?>" disabled/></td>
                <td id="opm"></td>
            </tr>
            <tr>
                <td>Current Address</td>
                <td>:</td>
                <td><textarea class="profile profilevalue" id="currentAdd" name="current_address" disabled><?php if(isset($alldata['current_address'])){ echo $alldata['current_address']; }?></textarea></td>
                <td id="cam"></td>
            </tr>
            <tr>
                <td>Permanent Address</td>
                <td>:</td>
                <td><textarea class="profile profilevalue" id="permanentAdd" name="permanent_address" disabled><?php if(isset($alldata['permanent_address'])){ echo $alldata['permanent_address']; }?></textarea></td>
                <td id="pam"></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>
                    <input type="button" name="btn" class="btn" id="btnEditProfile"  value="Edit">
                    <input type="submit" name="btn" class="btn" id="btnUpdateProfile" hidden value="Update">
                </td>
            </tr>
        </table>
    </div>
    </form>
	</div>
<script>
    $(document).ready(function(){
        $('#btnEditProfile').click(function(){
            $(this).hide();
            $('#btnUpdateProfile').show();
            $('.profilevalue').removeAttr('disabled');
            $('.profilevalue').removeClass('profile');

        });
        $('#profileIMG').click(function(){
            $('#profilePicUp').trigger("click");
        });

        $("#profilePicUp").change(function(e){
            var profilePicURL =URL.createObjectURL(e.target.files[0]);
            $("#profilePicPreview").empty().append('<img id="profileIMG" src="' + profilePicURL +'" />');
            $("#submitPic").empty().append('<br/><input type="submit" id="profileImgUp" name="btn" value="SAVE"/>');


            $('#profileIMG').click(function(){
                $('#profilePicUp').trigger("click");
            })

        });

    })
    $(document).ready(function(){
        var fn= 0,ln= 0,pp= 0,hp= 0,op= 0,ca= 0,pa=0;
        $('#firstName').focusout(function(){

            var userName=$(this).val();
            var ul=userName.length;
            if(ul>2 && ul<20){

                $('#fnm').empty();
                fn=0;


            }else{
                $('#fnm').empty().append('At least 3 character and not more then 20 character');
                fn=1;

            }
        })
        $('#lastName').focusout(function(){

            var userName=$(this).val();
            var ul=userName.length;
            if(ul>2 && ul<20){

                $('#lnm').empty();
                ln=0;


            }else{
                $('#lnm').empty().append('At least 3 character and not more then 20 character');
                ln=1;

            }
        })
        $('#homePhone').focusout(function(){

            var val=$(this).val();
            var ul=val.length;
            if(ul<=11 && ul>3 && $.isNumeric(val) || ul==0){

                $('#hpm').empty();
                hp=0;


            }else{
                $('#hpm').empty().append('4-11 Numeric Number');
                hp=1;

            }
        })
        $('#officePhone').focusout(function(){

            var val=$(this).val();
            var ul=val.length;
            if(ul<=11 && ul>3 && $.isNumeric(val) || ul==0){

                $('#opm').empty();
                op=0;


            }else{
                $('#opm').empty().append('4-11 Numeric Number');
                op=1;

            }
        })
        $('#personalPhone').focusout(function(){

            var val=$(this).val();
            var ul=val.length;
            if(ul==11 && $.isNumeric(val) || ul==0){

                $('#ppm').empty();
                pp=0;


            }else{
                $('#ppm').empty().append('11 Numeric Number');
                pp=1;

            }
        })
        $('#currentAdd').focusout(function(){

            var userName=$(this).val();
            var ul=userName.length;
            if(ul>6 && ul<30){

                $('#cam').empty();
                ca=0;


            }else{
                $('#cam').empty().append('6-30 Characters');
                ca=1;

            }
        })
        $('#permanentAdd').focusout(function(){

            var userName=$(this).val();
            var ul=userName.length;
            if(ul>6 && ul<30){

                $('#pam').empty();
                pa=0;


            }else{
                $('#pam').empty().append('6-30 Characters');
                pa=1;

            }
        })
        $('#btnUpdateProfile').click(function(e){

            if(fn==1 || ln==1 || pp==1 || hp==1 || op==1 || ca==1 || pa==1){
                e.preventDefault();
            }
        })
    })
</script>
</body>
</html>