<?php
session_start();
if(!isset($_SESSION['useridlogin'])){
    header('location:login.php');

}

?>
<html>
    <head>
        <title>User|Home</title>
        <script src="../../../../assets/jquery.js"></script>
        <link href="../../../../assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="../../../../assets/css/style.css" rel="stylesheet">
        <script src="../../../../assets/js/bootstrap.min.js"></script>
        <script src="../../../../assets/js/scripts.js"></script>

    </head>
    <body>
    <div class="col-md-12">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <center>
                <br/><br/><br/><br/>

                <a href="logout.php">
                    <button class="btn btn-default" type="button">
                        <em class="glyphicon glyphicon-log-out"></em> Logout
                    </button>
                </a>
                <a href="profile.php">
                    <button class="btn btn-default" type="button">
                        <em class="glyphicon glyphicon-certificate"></em> Your Profile
                    </button>
                </a>
                <br/><br/><br/><br/>

        <h1>User Dashboard</h1>
            </center>
        </div>
        <div class="col-md-3"></div>

    </div>

    </body>
</html>